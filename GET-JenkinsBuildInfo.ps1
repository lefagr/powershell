function global:GET-JenkinsBuildInfo{

<#

.NAME

	GET-JenkinsBuildInfo

.SYNOPSIS 

        Check the latest given Jenkins job status

.SYNTAX

	GET-JenkinsBuildInfo [[-Username] <string[]>] [-JenkinJob] <string> [<CommonParameters>]

.PARAMETER JenkinsJob

	The Jenkins job you want to monitor. It has to be case sensitive.
	In case there are spaces in the job name you have to use double 
	quotes ( " ).

.PARAMETER Username

	User to authentiate in jenkins API. 

.EXAMPLE

	GET-JenkinsBuildInfo -JenkinsJob "My Jenkins Job".

.DESCRIPTION

        This GET-JenkinsBuildInfo function will use the jenkins api 
        and based on the given jenkins job name it  will output the 
        status of the latest job and it will create proper exit codes 
        based on the Job status, compartible with monitoring systems.

.NOTES

	This script is expandable and it can get a history of results.
	File Name: checklatestjenkinsjobstatus.ps1
	Author: Lefteris Agianitis - lefagr@gmail.com 
	Requires: Powershell v3

#>

PARAM(

    [parameter(Mandatory=$False,
    ValueFromPipeline=$True,
    Position=0)]
    [STRING[]]$Username="",

    [Parameter(Mandatory=$True,Position=1)]
    [String]$JenkinsJob
    )
 
    $baseUrl = "http://jenkins.company.com/api/json";
    $projectJSON = (Invoke-RestMethod -Uri $baseUrl)
    
   
    
    $baseProjectUrl = "http://jenkins.company.com/job/$JenkinJob/api/json";
    
   
        $projectJson = (Invoke-RestMethod -Uri $baseProjectUrl);
    
        $projectJson.builds | select -first 1 | ForEach-Object{
    
            $buildUrl = $_.url + "api/json";
            $buildJson = (Invoke-RestMethod -Uri $buildUrl)
    
            $jobstatus = ($buildJson.result);
            if ($jobstatus -like '*SUCCESS*')
            {
                write-host "0:0:OK"
                exit 0
            }
            else
            {
                write-host "2:2:Job Failed"
                exit 2
            }

            $buildJson.changeSet.items | foreach-object {    
                
               If(![string]::IsNullOrEmpty($Username)){
                   If ($_.user -eq $Username)
                   {
                     "Check-in: {0}" -f $_.msg;
                   }
               }              
               Else
               {
                 "User: {0}" -f $_.user;
                 "Check-in: {0}" -f $_.msg;
               }
                
            }
    
        }
}
